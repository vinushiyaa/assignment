package B.lk;



public class Storybook extends Book{
		 
	Storybook() {
	        super(BookType.childstory);
	        construct();
	    }
	 
	    @Override
	    protected void construct() {
	        System.out.println("Building small car");
	        
	    }
	}