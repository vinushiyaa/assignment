package B.lk;

public abstract class Book {
	 
    public Book(BookType model) {
        this.model = model;
        arrangeParts();
    }
 
    private void arrangeParts() {
       
    }
 
    // subclass level processing in this method
    protected abstract void construct();
 
    private BookType model = null;
 
    public BookType getModel() {
        return model;
    }
 
    public void setModel(BookType model) {
        this.model = model;
    }
}


